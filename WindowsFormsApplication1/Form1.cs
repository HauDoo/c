﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        List<PTB2> list = new List<PTB2>();
        string path = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "//ptb2.txt";
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var source = new BindingSource();

            List<PTB2> list = PTB2.read(path);
            source.DataSource = list;
            dataGridView1.DataSource = source;
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void btnResult_Click(object sender, EventArgs e)
        {
            double a = Double.Parse(tbA.Text);
            double b = Double.Parse(tbB.Text);
            double c = Double.Parse(tbC.Text);

            double x = 0;
            double x1 = 0;
            double x2 = 0;

            double delta = (b * b) - (4 * a * c);

            if (a == 0 && b == 0 && c == 0)
            {
                lbInfor.Text = "Phương trình có vô số nghiệm";
                lb1.Text = "undefine";
                lb2.Text = "undefine";
            }
            else
            {
                if (a == 0)
                {
                    if (b == 0 && c != 0)
                    {
                        lbInfor.Text = "Phương trình vô nghiệm";
                        lb1.Text = "undefine";
                        lb2.Text = "undefine";
                    }
                    else
                    {
                        x = -c / b;
                        lbInfor.Text = "Phương trình có 1 nghiệm";
                        lb1.Text = Convert.ToString(x);
                        lb2.Text = Convert.ToString(x);
                    }
                }
                else
                {
                    if (delta < 0)
                    {
                        lbInfor.Text = "Phương trình vô nghiệm";
                        lb1.Text = "undefine";
                        lb2.Text = "undefine";
                    }
                    else if (delta == 0)
                    {
                        x = -b / 2 * a;
                        lbInfor.Text = "Phương trình có nghiệm kép";
                        lb1.Text = Convert.ToString(x);
                        lb2.Text = Convert.ToString(x);
                    }
                    else
                    {
                        x1 = (-b - (float)Math.Sqrt(delta)) / 2 * a;
                        x2 = (-b + (float)Math.Sqrt(delta)) / 2 * a;
                        lbInfor.Text = "Phương trình có 2 nghiệm";
                        lb1.Text = Convert.ToString(x1);
                        lb2.Text = Convert.ToString(x2);
                    }
                }

                string data = a + ";" + b + ";" + c + ";" + lb1.Text + ";" + lb2.Text + ";" + lbInfor.Text;
                PTB2.write(path, data);

                var source = new BindingSource();

                List<PTB2> list = PTB2.read(path);

                source.DataSource = list;
                dataGridView1.DataSource = source;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace WindowsFormsApplication1
{
    public class PTB2
    {
        public double a { set; get; }
        public double b { set; get; }
        public double c { set; get; }
        public string x1 { set; get; }
        public string x2 { set; get; }
        public string result { set; get; }

        public PTB2()
        { }

        public PTB2(double a, double b, double c, string x1, string x2, string result)
        {
            this.a = a;
            this.b = b;
            this.c = c;
            this.x1 = x1;
            this.x2 = x2;
            this.result = result;
        }

   
        public static void write(String path, String text)
        {
            using (StreamWriter write = File.AppendText(path))
            {
                write.WriteLine(text);
                write.Close();
            }
        }
      
        public static List<PTB2> read(String path)
        {
            List<PTB2> list = new List<PTB2>();

            if (File.Exists(path))
            {
                using (StreamReader stream = File.OpenText(path))
                {
                    string s = "";

                    while ((s = stream.ReadLine()) != null)
                    {
                        Console.WriteLine(s);

                        string[] data = s.Split(';');
                        list.Add(new PTB2(Convert.ToDouble(data[0]), Convert.ToDouble(data[1]),
                            Convert.ToDouble(data[2]), data[3], data[4], data[5]));

                    }
                }
            }
                        
            return list;
        }
    }
}
